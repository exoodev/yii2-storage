<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2018
 * @package
 * @version 1.0.0
 */

namespace exoo\storage\actions;

use Yii;
use yii\base\Action;
use yii\helpers\Inflector;
use yii\helpers\FileHelper;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\imagine\Image;
use yii2tech\ar\position\PositionBehavior;
use yii\web\BadRequestHttpException;
use yii\base\InvalidConfigException;
use yii\di\Instance;
use Imagine\Image\ManipulatorInterface;

/**
 * File Action class.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class FileAction extends Action
{
    /**
     * @var string class name of the model which will be handled by this action.
     * The model class must implement [[ActiveRecordInterface]].
     * This property must be set.
     */
    public $modelClass;
    /**
     * @var callable a PHP callable that will be called to return the model corresponding
     * to the specified primary key value. If not set, [[findModel()]] will be used instead.
     * The signature of the callable should be:
     *
     * ```php
     * function ($id, $action) {
     *     // $id is the primary key value. If composite primary key, the key values
     *     // will be separated by comma.
     *     // $action is the action object currently running
     * }
     * ```
     *
     * The callable should return the model found, or throw an exception if not found.
     */
    public $findModel;
    /**
     * @var callable a PHP callable.
     * The signature of the callable should be:
     *
     * ```php
     * function ($uploadedFile, $model) {
     *     // $uploadedFile is the uploaded file
     *     // $model
     * }
     * ```
     */
    public $afterUpload;
    /**
     * @var callable a PHP callable.
     * The signature of the callable should be:
     *
     * ```php
     * function ($uploadedFile, $model) {
     *     // $uploadedFile is the uploaded file
     *     // $model
     * }
     * ```
     */
    public $afterSave;
    /**
     * @var string the bucket name
     */
    public $bucketName;
    /**
     * @var array the bucket data
     */
    public $bucketData = [];
    /**
     * @var string the fileStorage
     */
    public $fileStorage;
    /**
     * @var string the fileStorage name
     */
    public $fileStorageName = 'fileStorage';
    /**
     * @var string the fileStorage Bucket
     */
    public $fileStorageBucket;
    /**
     * @var array the transform Images
     */
    public $transformImage = [];
    /**
     * @var string the file Attribute
     */
    public $fileAttribute = 'filename';
    /**
     * @var string name of relation (eg. 'images')
     */
    public $relation;
    /**
     * @var string name of relation (eg. 'product_id')
     */
    public $relationAttribute;
    /**
     * @var string the property
     */
    public $positionAttribute = 'position';
    /**
     * @var boolean the property
     */
    public $sortable = false;
    /**
     * @var boolean the Using the extension
     */
    public $use_extension = false;
    /**
     * @var string the property
     */
    public $prefixFile;
    /**
     * @var boolean the delete original image
     */
    public $deleteOriginalImage = true;
    /**
     * @var string the image for lightbox
     */
    public $previewFullsizeImage;
    /**
    * @var string the filename
    */
    private $_filename;
    /**
     * @var string the file extension
     */
    private $_extension;
    /**
    * @var string the property
    */
    private $_uploadedFile;
    /**
     * @var string the property
     */
    private $_isImage;
    /**
     * @var array the property
     */
    private $_thumbnailSizes = [];

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        if (!Yii::$app->request->isAjax) {
            return;
        }

        parent::init();

        if ($this->modelClass === null) {
            throw new InvalidConfigException(get_class($this) . '::$modelClass must be set.');
        }

        if (!is_object($this->fileStorageBucket)) {
            if ($this->bucketName === null) {
                throw new InvalidConfigException('"bucketName" must be set.');
            }

            $this->fileStorage = Instance::ensure($this->fileStorageName, 'exoo\storage\FileStorage');

            if ($this->bucketData) {
                $this->fileStorage->addBucket($this->bucketName, $this->bucketData);
            }
            $this->fileStorageBucket = $this->fileStorage->getBucket($this->bucketName);
        }

        if ($this->relation && $this->relationAttribute === null) {
            throw new InvalidConfigException('"relationAttribute" must be set.');
        }

        // set _thumbnailSizes
        if (!isset($this->transformImage['thumbnail']['preview'])) {
            $this->transformImage['thumbnail']['preview'] = ['width' => 150, 'height' => 150];
        }
        $this->_thumbnailSizes = array_keys($this->transformImage['thumbnail']);

        // set previewFullsizeImage
        if (!$this->previewFullsizeImage) {
            if ($this->_thumbnailSizes) {
                $this->previewFullsizeImage = $this->_thumbnailSizes[0];
            } elseif (!$this->deleteOriginalImage) {
                $this->previewFullsizeImage = 'original';
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function run($id)
    {
        if (!Yii::$app->request->isPost) {
            throw new BadRequestHttpException('Only POST is allowed');
        }

        $model = $this->findModel($id);
        $action = Yii::$app->request->post('action') . 'Action';

        if (method_exists($this, $action)) {
            return $this->{$action}($model);
        } else {
            throw new NotFoundHttpException("Object not found: $action");
        }
    }

    /**
     * List action
     * @param mixed $value
     * @return string the result
     */
    public function listAction($model)
    {
        $data = [];

        if ($this->relation) {
            $query = $this->getRelationModel($model)::find()->where([$this->relationAttribute => $model->id]);
            if ($this->sortable) {
                $query->orderBy($this->positionAttribute);
            }
            $nodes = $query->all();
            foreach ($nodes as $node) {
                $data[] = $this->prepareRequest($node->id, $node->{$this->fileAttribute});
            }
        } else {
            if ($model->{$this->fileAttribute}) {
                $data[] = $this->prepareRequest($model->id, $model->{$this->fileAttribute});
            }
        }

        return $this->controller->asJson($data);
    }

    protected function setPrefixFile()
    {
        if ($this->prefixFile !== false) {
            if ($this->prefixFile === null) {
                return '_(' . date("dmY") . '_' . date("His") . ')';
            } else {
                return $this->prefixFile . '_';
            }
        }
    }

    /**
     * Undocumented method
     * @param mixed $value
     * @return boolean the result
     */
    public function uploadAction($model)
    {
        $file = UploadedFile::getInstanceByName('file');
        $this->_isImage = strstr($file->type, '/', true) == 'image';


        if ($this->_isImage) {
            $this->_filename = uniqid(rand());
        } else {
            $this->_filename = Inflector::slug($file->baseName, '_');

            if ($this->fileStorageBucket->fileExists($this->_filename . '.' . $file->extension)) {
                $this->_filename = $this->_filename . $this->setPrefixFile();
            }
        }

        $filename = $this->_isImage ? $this->_filename . '_original' : $this->_filename;

        if ($file->name == 'blob') {
            $data = file_get_contents($file->tempName);
            $this->_extension = 'jpg';
            $uploaded = $this->fileStorageBucket->saveFileContent($filename . '.' . $this->_extension, $data);
        } else {
            $this->_extension = $file->extension;
            $uploaded = $this->fileStorageBucket->copyFileIn($file->tempName, $filename . '.' . $this->_extension);
        }

        if ($uploaded) {
            $this->_uploadedFile = $filename . '.' . $this->_extension;
            if ($this->afterUpload) {
                call_user_func($this->afterUpload, $this->_uploadedFile, $model);
            }
            $this->transformedImages();

            if ($data = $this->saveModel($model)) {
                if ($this->afterSave) {
                    call_user_func($this->afterSave, $this->_uploadedFile, $model);
                }
                return $this->controller->asJson($data);
            }
        }
    }

    /**
     * Undocumented method
     * @param mixed $value
     * @return string the result
     */
    public function deleteAction($model)
    {
        $result = [];

        if ($this->relation) {
            if ($file_id = Yii::$app->request->post('file_id')) {
                $relationModel = $this->getRelationModel($model)::findOne($file_id);
                $this->attachBehaviorToRelationModel($relationModel);
                if ($relationModel->delete()) {
                    $this->deleteFile($relationModel->{$this->fileAttribute});
                    $result = ['message' => true];
                }
            }
        } else {
            $oldFilename = $model->{$this->fileAttribute};
            $model->{$this->fileAttribute} = '';

            if ($this->use_extension) {
                $oldExtension = $model->extension;
                $model->extension = '';
            }

            if ($model->update(false)) {
                $this->deleteFile($oldFilename);
                $result = ['message' => true];
            }
        }

        return $this->controller->asJson($result);
    }

    /**
     * Delete file from bucket
     * @param string $file
     */
    protected function deleteFile($file)
    {
        $this->fileStorageBucket->deleteFile($file);

        // delete thumbnail images
        if (isset($this->transformImage['thumbnail'])) {
            $sizes = array_keys($this->transformImage['thumbnail']);
            $sizes[] = 'preview';

            foreach ($sizes as $size) {
                $parts = pathinfo($file);
                $filename = $parts['filename'] . '_' . $size . '.' . $parts['extension'];
                $this->fileStorageBucket->deleteFile($filename);
            }
        }

        return true;
    }

    /**
     * Undocumented method
     * @param mixed $value
     * @return string the result
     * ['mode' => 'ManipulatorInterface::THUMBNAIL_INSET']
     */
    protected function transformedImages()
    {
        if ($this->_isImage) {
            if ($this->transformImage) {
                foreach ($this->transformImage as $method => $options) {
                    if (method_exists($this, $method)) {
                        $this->{$method}($options);
                    }
                }
            }

            if ($this->_thumbnailSizes && $this->deleteOriginalImage) {
                $this->fileStorageBucket->deleteFile($this->_uploadedFile);
            }
        }
    }

    /**
     * Undocumented method
     * @param mixed $value
     * @return boolean the result
     */
    public function thumbnail($options)
    {

        if ($uploadedFile = $this->fileStorageBucket->getFullFileName($this->_uploadedFile)) {
            foreach ($options as $size => $option) {
                $newFilename = $this->_filename . '_' . $size . '.' . $this->_extension;

                // Modes //
                // ManipulatorInterface::THUMBNAIL_INSET
                // ManipulatorInterface::THUMBNAIL_OUTBOUND

                $width = ArrayHelper::getValue($option, 'width');
                $height = ArrayHelper::getValue($option, 'height');
                $mode = ArrayHelper::getValue($option, 'mode', ManipulatorInterface::THUMBNAIL_OUTBOUND);
                $quality = ArrayHelper::getValue($option, 'quality', 100);

                ini_set('memory_limit', '512M');

                $image = $this->addCanvasToImage($uploadedFile);
                Image::thumbnail($image, $width, $height, $mode)
                    ->save(dirname($uploadedFile) . '/' . $newFilename, ['quality' => $quality]);
            }
        }
    }

    protected function addCanvasToImage($uploadedFile)
    {
        $imagine = Image::getImagine();
        $image = $imagine->open(Yii::getAlias($uploadedFile));
        $size    = $image->getSize();
        $palette = new \Imagine\Image\Palette\RGB();
        $canvas  = $imagine->create(
            new \Imagine\Image\Box($size->getWidth(), $size->getHeight()),
            $palette->color('fff')
        );
        $canvas->paste($image, new \Imagine\Image\Point(0, 0));

        return $canvas;
    }

    /**
     * Undocumented method
     * @param array $options ['sizes' => [], 'start', 'watermarkImage']
     */
    public function watermark($options)
    {
        $sizes = [];

        if (isset($options['sizes'])) {
            foreach ((array) $options['sizes'] as $value) {
                if (isset($this->transformImage['thumbnail'][$value])) {
                    $sizes[] = $value;
                }
            }
        }

        $start = ArrayHelper::getValue($options, 'start');
        foreach ($sizes as $size) {
            $filename = $this->_filename . '_' . $size . '.' . $this->_extension;
            if ($file = $this->fileStorageBucket->getFullFileName($filename)) {
                Image::watermark($file, $options['watermarkImage'], $start)->save($file);
            }
        }
    }

    /**
     * Undocumented method
     * @param mixed $value
     * @return array the result
     */
    protected function saveModel($model)
    {
        $result = [];
        $id = null;
        $filename = $this->_filename . '.' . $this->_extension;

        if ($this->relation) {
            $relationModel = $this->getRelationModel($model);
            $relationModel->{$this->relationAttribute} = $model->id;
            $relationModel->{$this->fileAttribute} = $filename;
            if ($this->use_extension) {
                $relationModel->extension = $this->_extension;
            }
            if ($relationModel->save()) {
                $id = $relationModel->id;
            }
        } else {
            $oldFilename = $model->{$this->fileAttribute};
            if ($oldFilename) {
                $this->deleteFile($oldFilename);
            }

            $attr = [$this->fileAttribute => $filename];
            if ($this->use_extension) {
                $attr['extension'] = $this->_extension;
            }
            $model->updateAttributes($attr);
            $id = $model->id;
        }

        if ($id) {
            $result = $this->prepareRequest($id, $filename);
        }

        return $result;
    }

    protected function prepareRequest($id, $filename)
    {
        return array_filter([
            'id' => $id,
            'bucket' => $this->bucketName,
            'filename' => pathinfo($filename, PATHINFO_FILENAME),
            'extension' => pathinfo($filename, PATHINFO_EXTENSION),
            'fullsize' => $this->previewFullsizeImage,
            'url' => dirname($this->fileStorageBucket->getFileUrl($filename)),
            'sizes' => $this->_thumbnailSizes
        ]);
    }

    /**
     * Download action
     * @param string $filename
     * @return Response response.
     * @throws NotFoundHttpException if file does not exist.
     */
    public function downloadAction()
    {
        $filename = Yii::$app->request->post('filename');

        if (!$this->fileStorageBucket->fileExists($filename)) {
            throw new NotFoundHttpException("File '{$filename}' does not exist.");
        }

        $mimeType = FileHelper::getMimeTypeByExtension($filename);
        $handle = $this->fileStorageBucket->openFile($filename, 'r');

        return Yii::$app->getResponse()->sendStreamAsFile($handle, basename($filename), [
            'mimeType' => $mimeType
        ]);
    }

    /**
     * Sortable action
     * @param mixed $model
     */
    public function sortableAction($model)
    {
        if ($this->relation) {
            $id = Yii::$app->request->post('id');
            $position = Yii::$app->request->post('position');
            $relationModel = $this->getRelationModel($model)::findOne($id);
            $this->attachBehaviorToRelationModel($relationModel);
            $relationModel->moveToPosition($position);
        }
    }

    /**
     * Get relation model
     * @param mixed $model
     * @return string the result
     */
    protected function getRelationModel($model)
    {
        $relation = $model->getRelation($this->relation);
        $relationClassName = $relation->modelClass;
        $relationModel = new $relationClassName();
        $this->attachBehaviorToRelationModel($relationModel);

        return $relationModel;
    }

    /**
     * Undocumented method
     * @param mixed $value
     * @return string the result
     */
    protected function attachBehaviorToRelationModel($model)
    {
        if (!$this->sortable) {
            return;
        }

        $className = PositionBehavior::className();
        $behaviors = $model->getBehaviors();

        foreach ($behaviors as $key => $behavior) {
            if (get_class($behavior) == $className) {
                if (in_array($this->relationAttribute, $behavior->groupAttributes)) {
                    return;
                }
            }
        }

        $model->attachBehavior('positionBehavior', [
            'class' => $className,
            'positionAttribute' => $this->positionAttribute,
            'groupAttributes' => [$this->relationAttribute],
        ]);
    }

    /**
     * Returns the data model based on the primary key given.
     * If the data model is not found, a 404 HTTP exception will be raised.
     * @param string $id the ID of the model to be loaded. If the model has a composite primary key,
     * the ID must be a string of the primary key values separated by commas.
     * The order of the primary key values should follow that returned by the `primaryKey()` method
     * of the model.
     * @return ActiveRecordInterface the model found
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id)
    {
        if ($this->findModel !== null) {
            return call_user_func($this->findModel, $id, $this);
        }

        /* @var $modelClass ActiveRecordInterface */
        $modelClass = $this->modelClass;
        $keys = $modelClass::primaryKey();
        if (count($keys) > 1) {
            $values = explode(',', $id);
            if (count($keys) === count($values)) {
                $model = $modelClass::findOne(array_combine($keys, $values));
            }
        } elseif ($id !== null) {
            $model = $modelClass::findOne($id);
        }

        if (isset($model)) {
            return $model;
        }

        throw new NotFoundHttpException("Object not found: $id");
    }

    // protected function getFilenameWithPrefix($filename, $prefix)
    // {
    //     $name = pathinfo($filename, PATHINFO_FILENAME);
    //     $extension = pathinfo($filename, PATHINFO_EXTENSION);
    //     return $name . '_' . $prefix . '.' . $extension;
    // }
}
