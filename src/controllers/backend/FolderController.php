<?php

namespace exoo\storage\controllers\backend;

use yii\rest\ActiveController;
use exoo\storage\models\Folder;

/**
 *
 */
class FolderController extends ActiveController
{
    public $modelClass = Folder::class;
}
