<?php

namespace exoo\storage\widgets;

use yii\web\AssetBundle;

/**
 * Asset bundle for widget [[FileInput]].
 */
class FileInputAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@exoo/storage/assets';
    /**
     * @inheritdoc
     */
    public $js = [
        'js/fileInput.js',
    ];
    /**
     * @inheritdoc
     */
    public $css = [
        'css/fileInput.css',
    ];
    /**
     * @inheritdoc
     */
    public $depends = [
        // 'exoo\exookit\ExookitAsset',
        'exoo\cropper\CropperAsset',
    ];
}
