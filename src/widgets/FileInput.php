<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\storage\widgets;

use Yii;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\helpers\Html;
use yii\base\InvalidConfigException;
use yii\widgets\InputWidget;

/**
 * File Upload
 *
 * ```php
 * use exoo\widgets\FileInput;
 * <?= FileInput::widget([
 *     'model' => $model,
 *     'attribute' => 'filename',
 *     'multiple' => true,
 *     'url' => ['file'],
 * ]) ?>
 * ```
 *
 * You can also use this widget in an [[ActiveForm]] using the [[ActiveField::widget()|widget()]]
 * method, for example like this:
 *
 * ```php
 * use exoo\widgets\FileInput;
 * <?= $form->field($model, 'filename')->widget(FileInput::className(), [
 *     'multiple' => true,
 *     'url' => ['file'],
 * ]) ?>
 * ```
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class FileInput extends InputWidget
{
    /**
     * @var string the upload Url
     */
    public $url;
    /**
     * @var string the property
     */
    public $multiple = false;
    /**
     * @var string the property
     */
    public $relation;
    /**
     * @var string the property
     */
    public $sortable = false;
    /**
     * @var array the options for the underlying JS plugin.
     */
    public $clientOptions = [];

    /**
     * Initializes the widget.
     * If you override this method, make sure you call the parent implementation first.
     */
    public function init()
    {
        if ($this->url === null) {
            throw new InvalidConfigException('The "url" option is required.');
        }

        if (!$this->hasModel()) {
            throw new InvalidConfigException('The "model" and "attribute" option is required.');
        }

        if ($this->relation) {
            $relation = $this->model->getRelation($this->relation);
            $relationClassName = $relation->modelClass;
            $relationModel = new $relationClassName();
            $label = Html::activeLabel($relationModel, $this->attribute);

            if ($this->field) {
                $this->field->label($label);
            }

            $this->options['id'] = Html::getInputId($relationModel, $this->attribute);
        }

        if ($this->model->isNewRecord) {
            echo '<div class="uk-alert uk-alert-primary">' . Yii::t('storage', 'To upload a file, you must save the form.') . '</div>';
            return;
        }

        parent::init();
        $id = $this->options['id'];
        $url = Url::to($this->url);
        $view = $this->getView();

        $options = Json::htmlEncode(array_merge([
            'url' => Url::to([$url, 'id' => $this->model->id]),
            'multiple' => $this->multiple,
            'fileAttribute' => $this->attribute,
            'sortable' => $this->sortable
        ], $this->clientOptions));

        $view->registerJs("jQuery('#$id').fileInput($options)");
        FileInputAsset::register($view);
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        echo Html::tag('div', null, $this->options);
    }
}
