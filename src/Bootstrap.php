<?php

namespace exoo\storage;

use Yii;
use yii\base\BootstrapInterface;

/**
 * File extension bootstrap class.
 */
class Bootstrap implements BootstrapInterface
{
	/**
     * @inheritdoc
     */
	public function bootstrap($app)
	{
		$app->i18n->translations['storage'] = [
			'class' => 'yii\i18n\PhpMessageSource',
			'basePath' => '@exoo/storage/messages',
		];

		if ($app instanceof \yii\console\Application) {
			$app->setModules([
                'storage' => [
		            'class' => 'yii\base\Module',
		            'controllerNamespace' => 'exoo\storage\controllers\console'
		        ],
            ]);
        }
	}
}
