<?php

namespace exoo\storage\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "{{%file}}".
 *
 * @property int $id
 * @property string $filename
 * @property int $position
 * @property int $folder_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class FolderFile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%file}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['folder_id', 'position', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['filename'], 'string', 'max' => 255],
            [['folder_id'], 'exist', 'skipOnError' => true, 'targetClass' => Folder::class, 'targetAttribute' => ['folder_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('storage', 'ID'),
            'filename' => Yii::t('storage', 'Filename'),
            'position' => Yii::t('storage', 'Position'),
            'folder_id' => Yii::t('storage', 'Folder'),
            'created_at' => Yii::t('storage', 'Upload At'),
            'updated_at' => Yii::t('storage', 'Updated At'),
            'created_by' => Yii::t('storage', 'Uploader By'),
            'updated_by' => Yii::t('storage', 'Updated By'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::class,
            TimestampBehavior::class,
        ];
    }

    public function fields()
    {
        $fields = parent::fields();
        $fields['url'] = function() {
            return $this->getUrl();
        };
        return $fields;
    }

    /**
     * Declares a `has-one` relation with class [[User]].
     * @return User The related object of the class [[User]].
     */
    public function getUploader()
    {
        return $this->hasOne(Yii::$app->user->identityClass, ['id' => 'created_by']);
    }

    public function getUrl($prefix = null) {
        return Yii::$app->fileStorage->getFileUrl('images', $this->filename, $prefix);
    }
}
