<?php

namespace exoo\storage\models;

use Yii;
use yii2tech\ar\dynattribute\DynamicAttributeBehavior;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "{{%file_bucket}}".
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property string $data
 * @property string $bucket
 *
 * @property FolderFile[] $folderFile
 */
class Folder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%file_folder}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['bucket'], 'string', 'max' => 50],
            [['name', 'slug'], 'string', 'max' => 255],
            [['description', 'data'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('storage', 'ID'),
            'name' => Yii::t('storage', 'Name'),
            'description' => Yii::t('storage', 'Description'),
            'slug' => Yii::t('storage', 'Slug'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'sluggableBehavior' => [
                'class' => SluggableBehavior::class,
                'attribute' => 'name',
                'ensureUnique' => false,
                'immutable' => true,
            ],
            [
                'class' => DynamicAttributeBehavior::class,
                'saveDynamicAttributeDefaults' => false,
                'dynamicAttributeDefaults' => [],
            ],
        ];
    }

    public function extraFields()
    {
        return ['files'];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(FolderFile::class, ['folder_id' => 'id'])->orderBy('position');
    }
}
