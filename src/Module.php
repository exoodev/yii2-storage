<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2018
 * @package
 * @version 1.0.0
 */

namespace exoo\storage;

use Yii;

/**
 * File module.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Module extends \yii\base\Module
{
    /**
     * @var string the extension name.
     */
    public $name;
    /**
     * @var string the property
     */
    public $extensions = [
        'file' => 'zip|rar|doc|docx|xls|xlsx|pdf|txt',
        'image' => 'jpg|jpeg|png|gif|ico|webp|tiff',
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->name = Yii::t('storage', 'Files');
    }
}
