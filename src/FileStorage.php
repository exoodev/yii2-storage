<?php

namespace exoo\storage;

use Yii;
use yii\base\Component;
use yii\base\UnknownMethodException;
use yii\helpers\ArrayHelper;
use exoo\storage\models\Folder;

/**
 * Undocumented class.
 *
 * Description method
 *
 */
class FileStorage extends Component
{
    /**
     * @var string the property
     */
    public $storage;
    /**
     * @var array the file actions
     */
    public $actions;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->storage = Yii::createObject($this->storage);
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function __get($name)
    {
        return $this->storage->__get($name);
    }

    /**
     * @inheritdoc
     */
    public function __set($name, $value)
    {
        return $this->storage->__set($name, $value);
    }

    public function __call($name, $params)
    {
        if ($this->storage->hasMethod($name)) {
            return call_user_func_array([$this->storage, $name], $params);
        }
        throw new UnknownMethodException('Calling unknown method: ' . get_class($this) . "::$name()");
    }

    /**
     * Get params for controller action
     * @param string $key
     * @param array $options
     * @return array the result
     */
    public function action($key, $options = [])
    {
        $params = ArrayHelper::getValue($this->actions, $key);
        if (!isset($params['class'])) {
            $params['class'] = 'exoo\storage\actions\FileAction';
        }

        return array_merge($options, $params);
    }

    /**
     * Get url file
     * @param string $bucketName
     * @param string $filename
     * @param string $prefix
     * @return string the result
     */
    public function getFileUrl($bucketName, $filename, $prefix = null)
    {
        if ($this->hasBucket($bucketName)) {
            $bucket = $this->getBucket($bucketName);
            
            if ($prefix) {
                $filename = $this->getFilenameWithPrefix($filename, $prefix);
            }

            return $bucket->getFileUrl($filename);
        }
    }

    public function getFilenameWithPrefix($filename, $prefix)
    {
        $name = pathinfo($filename, PATHINFO_FILENAME);
        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        return $name . '_' . $prefix . '.' . $extension;
    }

    /**
     * Get folder files by slug
     * @param string $bucketName
     * @param string $slug
     * @param string $prefix
     * @return array the result
     */
    public function getFileUrls($bucketName, $slugFolder, $prefix = null)
    {
        $bucket = $this->getBucket($bucketName);
        $folder = Folder::find()->where(['slug' => $slugFolder])->one();

        if ($folder->files) {
            $items = [];
            foreach ($folder->files as $key => $file) {
                if ($prefix) {
                    if (is_array($prefix)) {
                        foreach ($prefix as $name) {
                            if ($url = $this->getFileUrl($bucketName, $file->filename, $name)) {
                                $items[$key][$name] = $url;
                            }
                        }
                    } else {
                        $items[$key][$prefix] = $this->getFileUrl($bucketName, $file->filename, $prefix);
                    }
                } else {
                    $items[$key]['url'] = $this->getFileUrl($bucketName, $file->filename, $prefix);
                }
                $items[$key]['filename'] = $file->filename;
            }

            return $items;
        }
    }
}
