<?php
return [
    'navside' => [
        [
            'icon' => '<i class="fas fa-image fa-lg fa-fw uk-margin-small-right"></i>',
            'label' => Yii::t('storage', 'Images'),
            'url' => ['/storage/image/index'],
            'routeActive' => 'storage/image',
        ],
        [
            'icon' => '<i class="fas fa-file-upload fa-lg fa-fw uk-margin-small-right"></i>',
            'label' => Yii::t('storage', 'Files'),
            'url' => ['/storage/file/index'],
            'routeActive' => 'storage/file',
        ],
    ]
];
