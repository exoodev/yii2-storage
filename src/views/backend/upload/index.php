<div class="js-upload uk-placeholder uk-text-center">
    <span uk-icon="icon: cloud-upload"></span>
    <span class="uk-text-middle">Attach binaries by dropping them here or</span>
    <div uk-form-custom>
        <input type="file" multiple>
        <span class="uk-link">selecting one</span>
    </div>
</div>
<progress id="js-progressbar" class="uk-progress" value="0" max="100" hidden></progress>

<?php
$js = <<<JS
var bar = document.getElementById('js-progressbar');

    UIkit.upload('.js-upload', {

        url: '/system/user/upload/file',
        multiple: true,
        name: 'Upload[file]',

        error: function () {
            console.log('error', arguments);
        },

        loadStart: function (e) {
            bar.removeAttribute('hidden');
            bar.max = e.total;
            bar.value = e.loaded;
        },

        progress: function (e) {
            bar.max = e.total;
            bar.value = e.loaded;
        },

        loadEnd: function (e) {
            bar.max = e.total;
            bar.value = e.loaded;
        },

        completeAll: function () {
            setTimeout(function () {
                bar.setAttribute('hidden', 'hidden');
            }, 1000);
        }

    });
JS;
$this->registerJs($js);
