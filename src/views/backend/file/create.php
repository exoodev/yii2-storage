<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model exoo\storage\models\Folder */

$this->title = Yii::t('storage', 'New folder');
?>
<?= $this->render('_form', [
    'model' => $model,
]) ?>
