<?php

use yii\helpers\Url;
use yii\helpers\Html;
use exoo\widgets\FileInput;

$this->title = Yii::t('storage', 'Files');
$url = Url::to(['index']);
?>

<div class="uk-card uk-card-default">
    <div class="uk-card-header">
        <div class="uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
            <div>
                <h4 class="uk-margin-remove">
                    <a href="<?= $url ?>"><?= Html::encode($this->title) ?></a> <i class="fas fa-angle-right fa-sm uk-text-muted uk-margin-left uk-margin-right"></i> <?= Html::encode($model->name) ?>
                </h3>
            </div>
            <div>
                <?=  Html::a(Yii::t('storage', 'Back'), ['index'], [
                    'class' => 'uk-button uk-button-default',
                ]) ?>
                <?=  Html::a(Yii::t('storage', 'Update'), ['update', 'id' => $model->id], [
                    'class' => 'uk-button uk-button-primary',
                ]) ?>
                <?=  Html::a(Yii::t('storage', 'Delete'), ['delete', 'id' => $model->id], [
                    'data-method' => 'post',
                    'data-confirm' => Yii::t('storage', 'Are you sure want to delete this folder?'),
                    'class' => 'uk-button uk-button-danger',
                ]) ?>
            </div>
        </div>
    </div>
    <div class="uk-card-body">
        <?= FileInput::widget([
            'model' => $model,
            'attribute' => 'filename',
            'multiple' => true,
            'url' => ['file'],
            'clientOptions' => [
                'extensions' => '*.(' . $this->context->module->extensions['file'] . ')',
            ]
        ]) ?>
    </div>
    <div class="uk-card-footer">
        <span><?= Yii::t('system', 'Slug') ?></span>
        <code uk-tooltip="<?= Yii::t('system', 'Slug') ?>"><?= Html::encode($model->slug) ?></code>
    </div>
</div>
