<?php

use yii\helpers\Url;
use yii\helpers\Html;
use exoo\widgets\ActiveForm;

?>

<div class="uk-card uk-card-default uk-card-body uk-card-small">
    <?php $form = ActiveForm::begin(); ?>
        <div class="uk-margin-bottom uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
            <div>
                <h3 class="uk-card-title uk-margin-remove"><?= Html::encode($this->title) ?></h3>
            </div>
            <div>
                <?=  Html::submitButton(Yii::t('storage', 'Save'), [
                    'class' => 'uk-button uk-button-primary',
                ]) ?>
                <?=  Html::a(Yii::t('storage', 'Close'), Url::previous(), [
                    'class' => 'uk-button uk-button-default',
                ]) ?>
                <?php  if (!$model->isNewRecord): ?>
                    <?=  Html::a(Yii::t('storage', 'Delete'), ['delete', 'id' => $model->id], [
                        'data-method' => 'post',
                        'data-confirm' => Yii::t('storage', 'Are you sure want to delete this folder?'),
                        'class' => 'uk-button uk-button-danger',
                    ]) ?>
                <?php  endif; ?>
            </div>
        </div>
        <div class="uk-width-1-2@m">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'description')->textarea(['rows' => 7]) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
