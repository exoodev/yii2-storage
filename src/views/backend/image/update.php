<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model exoo\storage\models\Folder */

$this->title = Yii::t('storage', 'Update folder') . ': ' . Html::encode($model->name);
?>
<?= $this->render('_form', [
    'model' => $model,
]) ?>
