<?php

use yii\helpers\Url;
use yii\helpers\Html;

$url = Url::to(['view', 'id' => $model->id]);
?>

<a class="ex-storage-item" href="<?= $url ?>">
    <div class="uk-card uk-card-default uk-card-small uk-box-shadow-small uk-box-shadow-hover-medium">
        <div class="uk-card-badge uk-label">
            <i class="far fa-clone uk-margin-small-right"></i>
            <span><?= Html::encode(count($model->files)) ?></span>
        </div>
        <div class="uk-card-media-top">
            <div class="uk-cover-container uk-height-small">
                <?php if ($first = current($model->files)): ?>
                    <img src="<?= $first->getUrl('medium') ?>" uk-cover>
                <?php endif; ?>
            </div>
        </div>
        <div class="uk-card-footer">
            <h3 class="uk-h5 uk-text-truncate uk-flex uk-flex-middle">
                <i class="far fa-folder fa-lg uk-margin-small-right uk-text-muted"></i>
                <span><?= Html::encode($model->name) ?></span>
            </h3>
        </div>
    </div>
</a>

<?php
$css = <<<CSS
.ex-storage-item .uk-card-badge {
    top: 10px;
    right: 10px;
    background: rgba(0, 0, 0, 0.36);
    padding: 2px 5px;
}
.ex-storage-item .uk-card-small .uk-card-footer {
    padding: 10px 15px;
}
CSS;
$this->registerCss($css);
