var obj = {
    init: function (options, elem) {
        this.options = $.extend({}, this.options, options);
        this.elem = elem;
        this.$elem = $(elem);
        var $this = this;

        var template = this._template();
        // template = template.replace(/\{:columns}/g, this.options.columns);

        this.fileupload = $(template);
        this.placeholder = this.fileupload.find('.uk-placeholder');
        this.preview = this.fileupload.find('.ex-fileupload-preview').children();
        this.button = this.fileupload.find('.ex-fileupload-add');
        this.input = this.fileupload.children('input:file');

        this._build();
        this._dropEvent();

        if (this.options.multiple) {
            this.input.prop('multiple', true);
            this._sortable();
        }

        this.button.on('click', function(e) {
            $this.input.trigger('click');
        });

        this.input.on('change', function(e) {
            $this._selectedFiles(e.target.files)
        });

        this.preview.on('click', '.ex-fileupload-download', function(e) {
            e.preventDefault();
            var filename = $(this).data('filename');
            $.ajax({
                url: $this.options.url,
                data: {
                    action: 'download',
                    filename
                },
                method: 'POST',
                xhrFields: {
                    responseType: 'blob'
                },
                success: function(data) {
                    var a = document.createElement('a');
                    var url = window.URL.createObjectURL(data);
                    a.href = url;
                    a.download = filename;
                    a.click();
                    window.URL.revokeObjectURL(url);
                }
            });
        });

        this.preview.on('click', '.ex-fileupload-copy-shortcode', function(e) {
            e.preventDefault();
            var filename = $(this).data('filename');
            var size = $(this).data('size');
            var bucket = $(this).data('bucket');
            ex.copyToClipboard('[file bucket="'+bucket+'" name="'+filename+'" size="'+size+'"]');
            UIkit.notification($this.options.label.copy, 'success');
        });

        this._renderOldFiles();

        return this;
    },

    options: {
        url: '/file',
        // Allow multiple files to be uploaded.
        multiple: false,
        // File MIME type filter. (eg. image/*) | default - false
        mime: false,
        // Max size in Bytes | default - 0
        maxSize: 0,
        method: 'POST',
        // File name filter. (eg. '*.(zip|jpg|png)') | default - false
        extensions: false,
        // 1-6
        // columns: 6,
        label: {
            add: 'Выбрать',
            crop: 'Ок',
            cancel: 'Отмена',
            rotate_left: 'Повернуть влево',
            rotate_right: 'Повернуть вправо',
            zoom_in: 'Увеличить',
            zoom_out: 'Уменьшить',
            reset: 'Сброс',
            delete: 'Удалить',
            download: 'Скачать',
            copyShortcode: 'Скопировать шорткод',
            copy: 'Скопировано',
            width: 'Ширина',
            height: 'Высота',
            mirroringH: 'Отражение по горизонтали',
            mirroringV: 'Отражение по вертикали'
        },
        addButtonClass: 'uk-button uk-button-primary',
        crop: true,
        cropperOptions: {},
        dragoverClass: 'uk-dragover',
        msgInvalidMime: 'Разрешены только MIME: %s',
        msgInvalidName: 'Разрешены только: %s',
        msgInvalidSize: 'Файл слишком большой: %s Байт Макс.',
        msgInvalidFile: 'Неверный файл',
        alertValidate: true,
        preview: {
            height: 150,
            width: 150,
        },
        sortable: false,
        // fit, full
        cropModalSize: 'fit',
        imageExtensions: ['jpg','png','gif','jpeg','ico']
    },

    _template: function() {
        return [
            '<div class="ex-fileupload">',
                '<div class="ex-fileupload-preview">',
                    '<div class="uk-child-width-auto@m uk-flex uk-flex-middle uk-text-center uk-grid-small" uk-grid></div>',
                '</div>',
                '<button type="button" class="ex-fileupload-add uk-margin-small-top"></button>',
                '<input class="uk-hidden" type="file">',
            '</div>',
        ].join('');
    },

    _itemTemplate: function() {
        return [
            '<div class="uk-card uk-card-default uk-animation-scale-up uk-box-shadow-small uk-box-shadow-hover-medium">',
                '<div class="ex-fileupload-item-nav uk-position-top">',
                    '<ul class="uk-iconnav">',
                        '<li><a class="ex-fileupload-copy-shortcode" href="#" uk-icon="copy" uk-tooltip="' + this.options.label.copyShortcode + '"></a></li>',
                        '<li><a class="ex-fileupload-download" href="#" uk-icon="cloud-download" uk-tooltip="' + this.options.label.download + '"></a></li>',
                        '<li><a class="ex-fileupload-delete" href="#" uk-icon="trash" uk-tooltip="' + this.options.label.delete + '"></a></li>',
                    '</ul>',
                '</div>',
                '<div class="ex-fileupload-item-body uk-flex uk-flex-center uk-flex-middle"></div>',
                '<div class="ex-fileupload-item-footer uk-position-bottom"></div>',
            '</div>',
        ].join('');
    },

    _renderOldFiles: function() {
        var $this = this;
        $.post(this.options.url, {action: 'list'}).done(function(data) {
            if (data.length) {
                $this.preview.empty();
                $.each(data, function(index, file) {
                    $this._renderPreviewItem(file);
                });
            } else {
                $this._renderDropIcon();
            }
        });
    },

    _upload: function(file) {
        var formData = new FormData();
        formData.append('file', file);
        formData.append('action', 'upload');

        var request = $.ajax(this.options.url, {
            method: this.options.method,
            data: formData,
            processData: false,
            contentType: false,
            error: function() {
                console.log('error', arguments);
            },
        });

        this._renderPreloadItem(request);
    },

    _renderPreloadItem: function(request) {
        var $this = this;
        this._renderDropIcon(false);

        if (!this.options.multiple) {
            this.preview.empty();
        }

        var item = $(this._itemTemplate());
        var itemBody = item.find('.ex-fileupload-item-body');
        var ratio = this.options.preview.height / 100;
        var spinner = $('<div uk-spinner="ratio: ' + (this.options.preview.height / 100) + '"></div>');

        itemBody.css(this.options.preview).html(spinner);
        this.preview.append($('<div>').html(item));

        request.done(function(data) {
            if (data) {
                itemBody.empty();
                $this._renderPreviewItem(data, item);
            } else {
                item.parent().remove();
                $this._renderDropIcon();
                $this._alert('Error loading on server', 'danger');
            }
        });
    },

    _getUrls: function(data) {
        var path = data.url + '/'
        var filename = data.filename
        var extension = '.' + data.extension
        var isImage = this._isImage(data.extension)
        var urls = {}

        if (data.sizes) {
            $.each(data.sizes, function(index, size) {
                urls[size] = path + filename + '_' + size + extension
            })
            urls.fullsize = urls[data.fullsize]
            urls.download = isImage ? filename + '_' + data.fullsize + extension : filename + extension
        }

        return urls
    },

    _isImage: function(extension) {
        return $.inArray(extension, this.options.imageExtensions) > -1
    },

    _renderPreviewItem: function(data, item_preload) {
        var $this = this;
        var item;
        var itemContent;
        var filename = data.filename;
        var extension = data.extension;
        var url = this._getUrls(data)

        if (item_preload) {
            item = item_preload;
        } else {
            item = $(this._itemTemplate());
        }

        item.find('.ex-fileupload-copy-shortcode').data({
            filename: filename + '.' + extension,
            size: data.fullsize,
            bucket: data.bucket
        });
        item.find('.ex-fileupload-download').data('filename', url.download);

        var itemNav = item.find('.ex-fileupload-item-nav');
        var itemBody = item.find('.ex-fileupload-item-body');
        var itemFooter = item.find('.ex-fileupload-item-footer');
        itemFooter.find('code').html(data.id)

        if (this.options.sortable) {
            itemBody.addClass('uk-drag');
        }

        if (this._isImage(extension)) {
            itemContent = $('<img class="uk-animation-scale-up">');
            itemContent.css(this.options.preview).css('object-fit', 'cover').attr('src', url.preview);
            var navButton = $('<li uk-lightbox="animation: fade" uk-tooltip="' + this.options.label.zoom_in + '"><a href="' + url.fullsize + '" uk-icon="icon: search" data-caption="' + filename + '"></a></li>');
            itemNav.children().prepend(navButton);
        } else {
            switch (extension) {
                case 'txt':
                    iconClass = 'far fa-file-alt';
                    break;
                case 'pdf':
                    iconClass = 'far fa-file-pdf';
                    break;
                case 'zip':
                case 'rar':
                    iconClass = 'far fa-file-archive';
                    break;
                case 'doc':
                case 'docx':
                    iconClass = 'far fa-file-word';
                    break;
                case 'xls':
                case 'xlsx':
                    iconClass = 'far fa-file-excel';
                    break;
                case 'csv':
                    iconClass = 'far fa-file-csv';
                    break;
                default:
                    iconClass = 'far fa-file';
            }

            itemContent = $([
                '<div class="">',
                '<i class="' + iconClass + ' fa-3x"></i>',
                '<div class="uk-position-bottom uk-text-break">' + filename + '</div>',
                '</div>',
            ].join(''));
        }

        item.data('key', data.id);
        itemBody.css(this.options.preview).append(itemContent);

        item.hover(function() {
            itemNav.fadeIn(300)
            itemFooter.fadeIn(300)
        }, function() {
            itemNav.fadeOut(300)
            itemFooter.fadeOut(300)
        });

        item.on('click', '.ex-fileupload-delete', function(e) {
            e.preventDefault();
            $.post($this.options.url, {
                action: 'delete',
                file_id: item.data('key')
            }).done(function(data) {
                if (data.message === true) {
                    item.parent().addClass('uk-animation-scale-up uk-animation-reverse');
                    setTimeout(function() {
                        item.parent().remove();
                        if ($this.preview.is(':empty')) {
                            $this._renderDropIcon(true);
                        }
                    }, 500);
                }
            });
        });

        if (!item_preload) {
            $this.preview.append($('<div>').html(item));
        }
    },

    _match: function(pattern, path) {
        return path.match(new RegExp(("^" + (pattern.replace(/\//g, '\\/').replace(/\*\*/g, '(\\/[^\\/]+)*').replace(/\*/g, '[^\\/]+').replace(/((?!\\))\?/g, '$1.')) + "$"), 'i'));
    },

    _alert: function(message, type) {
        UIkit.notification({
            message: message,
            status: type || 'primary',
            pos: 'top-right',
            timeout: 50000
        });
    },

    _validate: function(file) {
        var filename = '<span class="uk-text-small">' + file.name + '</span><br>';
        var validate = true;
        var message;

        if (!file.type) {
            message = this.options.msgInvalidFile + ' ' + file.name;
            validate = false;
        }

        if (this.options.maxSize && this.options.maxSize * 1000 < file.size) {
            message = filename + this.options.msgInvalidSize.replace('%s', this.options.maxSize);
            validate = false;
        }
        if (this.options.extensions && !this._match(this.options.extensions, file.name)) {
            message = filename + this.options.msgInvalidName.replace('%s', this.options.extensions);
            validate = false;
        }
        if (this.options.mime && !this._match(this.options.mime, file.type)) {
            message = filename + this.options.msgInvalidMime.replace('%s', this.options.mime);
            validate = false;
        }
        if (!validate && this.options.alertValidate) {
            this._alert(message, 'danger');
        }
        return validate;
    },

    _cropModalDialog: function(id, url, options) {
        var $this = this;
        var cropper;
        var options = $.extend({}, {
            bgClose: false
        }, options);
        var dialog = UIkit.modal([
            '<div id="' + id + '" class="uk-modal ex-fileupload-modal uk-invisible">',
                '<div class="uk-modal-dialog uk-text-center">',
                    '<div class="uk-modal-body">',
                        '<div class="ex-fileupload-cropper"></div>',
                    '</div>',
                    '<div class="uk-modal-footer uk-flex uk-flex-middle uk-flex-between">',
                        '<div class="uk-text-left">',

                            '<a class="uk-icon-button uk-button-primary" data-cropper="rotate" data-option="-45" uk-tooltip="' + this.options.label.rotate_left + '"><i class="fas fa-undo-alt"></i></a>',
                            '<a class="uk-icon-button uk-button-primary" data-cropper="rotate" data-option="45" uk-tooltip="' + this.options.label.rotate_right + '"><i class="fas fa-redo-alt" data-fa-transform="flip-h"></i></a>',
                            '<a class="uk-icon-button uk-button-primary" data-cropper="zoom" data-option="0.1" uk-tooltip="' + this.options.label.zoom_in + '"><i class="fas fa-search-plus"></i></a>',
                            '<a class="uk-icon-button uk-button-primary" data-cropper="zoom" data-option="-0.1" uk-tooltip="' + this.options.label.zoom_out + '"><i class="fas fa-search-minus"></i></a>',
                            '<a class="uk-icon-button uk-button-primary" data-cropper="scaleX" data-option="-1" uk-tooltip="' + this.options.label.mirroringH + '"><i class="fas fa-arrows-alt-h"></i></a>',
                            '<a class="uk-icon-button uk-button-primary" data-cropper="scaleY" data-option="-1" uk-tooltip="' + this.options.label.mirroringV + '"><i class="fas fa-arrows-alt-v"></i></a>',
                            '<a class="uk-icon-button uk-button-danger uk-button-small" data-cropper="reset" uk-tooltip="' + this.options.label.reset + '"><i class="fas fa-times"></i></a>',

                            '<div class="uk-inline uk-margin-left uk-margin-small-right">',
                                '<span class="uk-form-icon">' + this.options.label.width.charAt(0) + ':</span>',
                                '<input class="uk-input" type="text" data-set="width" uk-tooltip="' + this.options.label.width + '">',
                            '</div>',
                            '<div class="uk-inline uk-margin-small-right">',
                                '<span class="uk-form-icon">' + this.options.label.height.charAt(0) + ':</span>',
                                '<input class="uk-input" type="text" data-set="height" uk-tooltip="' + this.options.label.height + '">',
                            '</div>',

                        '</div>',
                        '<div>',
                            '<button type="button" class="uk-button uk-button-default uk-modal-close">' + this.options.label.cancel + '</button> ',
                            '<a class="uk-button uk-button-primary ex-fileupload-crop">' + this.options.label.crop + '</a>',
                        '</div>',
                    '</div>',
                '</div>',
            '</div>',
        ].join(''), options);
        var $dialog = $(dialog.$el);
        var image = $('<img class="uk-hidden uk-animation-scale-up" src="' + url + '">');
        $dialog.find('.ex-fileupload-cropper').append(image);
        dialog.show();

        $dialog.on('shown', function() {

            if ($this.options.cropModalSize == 'fit') {
                $dialog.children().css('width', 'max-content');
            } else if ($this.options.cropModalSize == 'full') {
                $dialog.addClass('uk-modal-full');
            }

            // resize
            var containerHeight = $dialog.height();
            var footerHeight = $dialog.find('.uk-modal-footer').outerHeight();
            var body = $dialog.find('.uk-modal-body');
            image.css('max-height', containerHeight - footerHeight - body.outerHeight()).removeClass('uk-hidden');

            $dialog.removeClass('uk-invisible');
            setTimeout(function() {
                var options = $.extend({}, {
                    aspectRatio: 1,
                    viewMode: 1,
                    autoCropArea: 1,
                    crop: function (e) {
                        var data = e.detail;
                        $dialog.find('[data-set="width"]').val(Math.round(data.width));
                        $dialog.find('[data-set="height"]').val(Math.round(data.height));
                    },
                }, $this.options.cropperOptions);
                cropper = new Cropper(image.get(0), options);
            }, 700);
        }).on('hidden', function(ref) {
            if (ref.target === ref.currentTarget) {
                $dialog.remove();
                cropper.destroy();
                cropper = null;
            }
        });

        $dialog.on('click', '[data-cropper]', function(e) {
            e.preventDefault();
            var action = $(this).data('cropper');
            var value = $(this).data('option') || '';
            cropper[action](value);

            switch (action) {
                case 'scaleX':
                case 'scaleY':
                    $(this).data('option', -value);
                    break;
                default:
            }
        });

        $dialog.on('change', '[data-set]', function(e) {
            var data = {};
            var side = $(this).data('set');
            data[side] = Math.round($(this).val());
            cropper.setData(data);
        });

        $dialog.on('click', '.ex-fileupload-crop', function(e) {
            var canvas = cropper.getCroppedCanvas();
            canvas.toBlob(function(blob) {
                $this._upload(blob);
            })
            dialog.hide();
        });

        return dialog;
    },

    _renderCropImages: function(nodes) {
        var $this = this;
        var total = nodes.length;
        $.each(nodes, function(index, node) {
            var id = 'ex-fu';

            if (index == 0) {
                $this._cropModalDialog(id + index, node.url);
            } else {
                var el = '#' + id + (index - 1);
                $(document).on('hidden', el, function(e) {
                    e.preventDefault();
                    e.target.blur();
                    $this._cropModalDialog(id + index, node.url);
                    $(document).off('hidden', el);
                });
            }
        });
    },

    _selectedFiles: function(files) {
        var $this = this;
        if (!files.length) return;

        var url = function(file) {
            if (URL) {
                return URL.createObjectURL(file);
            } else if (FileReader) {
                var reader = new FileReader();
                reader.onload = function() {
                    return reader.result;
                };
                reader.readAsDataURL(file);
            }
        }
        var nodes = [];
        var cropImages = [];

        $.each(files, function(index, file) {
            if (!$this._validate(file)) return;

            if ($this.options.crop && file.type.match('image.*')) {
                cropImages.push({
                    url: url(file),
                    type: file.type
                });
            } else {
                $this._upload(file);
            }
        });

        if (cropImages.length) {
            this._renderCropImages(cropImages);
        }
        this.input.val('');
    },

    _dropEvent: function() {
        var $this = this;
        var hasdragCls = false;
        var dropContainer = this.preview.parent();

        dropContainer.on('drop', function(e) {
            if (e.originalEvent.dataTransfer && e.originalEvent.dataTransfer.files) {
                e.stopPropagation();
                e.preventDefault();

                $this.preview.parent().removeClass($this.options.dragoverClass);
                $this._selectedFiles(e.originalEvent.dataTransfer.files);
            }
        }).on('dragenter', function(e) {
            e.stopPropagation();
            e.preventDefault();
        }).on('dragover', function(e) {
            e.stopPropagation();
            e.preventDefault();

            if (!hasdragCls) {
                $this.preview.parent().addClass($this.options.dragoverClass);
                hasdragCls = true;
            }
        }).on('dragleave', function(e) {
            e.stopPropagation();
            e.preventDefault();
            $this.preview.parent().removeClass($this.options.dragoverClass);
            hasdragCls = false;
        });
    },

    _renderDropIcon: function(action = true) {
        if (action && this.preview.is(':empty')) {
            var icon = $('<span uk-icon="icon: move; ratio: 2"></span>');
            var item = $('<div class="uk-flex uk-flex-center uk-flex-middle uk-text-muted uk-animation-scale-up uk-align-center">');
            item.css(this.options.preview).append(icon);
            this.preview.append($('<div class="uk-width-1-1">').html(item));
        } else {
            this.preview.children('.uk-width-1-1').remove();
        }
    },

    _sortable: function() {
        if (!this.options.sortable) {
            return;
        }

        var $this = this;
        this.preview.attr('uk-sortable', 'handle: .uk-card');

        UIkit.util.on(this.preview, 'moved', function(e) {
            var item = $(e.detail[1])
            var index = item.index() + 1;
            var id = item.children().data('key');
            $.post($this.options.url, {
                action: 'sortable',
                id: id,
                position: index
            })
        });
    },

    _build: function() {
        this.$elem.append(this.fileupload);
        this.button.addClass(this.options.addButtonClass).html(this.options.label.add);

        if (this.options.mime) {
            this.input.attr('accept', this.options.mime);
        }
    },
};

// Object.create support test, and fallback for browsers without it
if ( typeof Object.create !== "function" ) {
    Object.create = function (o) {
        function F() {}
        F.prototype = o;
        return new F();
    };
}
// Create a plugin based on a defined object
$.plugin = function( name, object ) {
    $.fn[name] = function( options ) {
        return this.each(function() {
            if ( ! $.data( this, name ) ) {
                $.data( this, name, Object.create(object).init(
                    options, this ) );
                }
            });
        };
    };
    // Register the plugin
    $.plugin('fileInput', obj);
