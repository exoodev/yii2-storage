<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2018
 * @package
 * @version 1.0.0
 */

namespace exoo\storage\behaviors;

use Yii;
use yii\base\Behavior;
use yii\db\BaseActiveRecord;
use yii\di\Instance;

/**
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class FileBehavior extends Behavior
{
    /**
     * @var string the file Attribute
     */
    public $fileAttribute = 'filename';
    /**
     * @var string the file Attribute
     */
    public $extensionAttribute = 'extension';
    /**
     * @var string the bucket name
     */
    public $bucketName;
    /**
     * @var array the bucket data
     */
    public $bucketData = [];
    /**
     * @var string the filestorage
     */
    public $fileStorage = 'fileStorage';
    /**
     * @var string the fileStorage Bucket
     */
    public $fileStorageBucket;
    /**
     * @var string name of relation (eg. 'images')
     */
    public $relation;
    /**
     * @var string name of relation (eg. 'product_id')
     */
    public $relationAttribute;
    /**
     * @var string the property
     */
    public $transformImage = [];
    /**
     * @var boolean the Using the extension
     */
    public $use_extension = false;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        if (!is_object($this->fileStorageBucket)) {
            $fileStorage = Instance::ensure($this->fileStorage, 'exoo\storage\FileStorage');
            if ($this->bucketData) {
                $fileStorage->addBucket($this->bucketName, $this->bucketData);
            }
            $this->fileStorageBucket = $fileStorage->getBucket($this->bucketName);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function events()
    {
        return [
            BaseActiveRecord::EVENT_BEFORE_DELETE => 'deleteItem'
        ];
    }

    /**
     * Undocumented method
     * @param mixed $value
     * @return string the result
     */
    public function deleteItem($event)
    {
        if ($this->relation) {
            foreach ($this->owner->{$this->relation} as $model) {
                if ($this->deleteFile($model->{$this->fileAttribute})) {
                    $model->delete();
                }
            }
        } else {
            if ($this->deleteFile($this->owner->{$this->fileAttribute})) {
                $attr = [
                    $this->fileAttribute => null,
                ];

                if ($this->use_extension) {
                    $attr['extension'] = null;
                }

                $this->owner->updateAttributes($attr);
            }
        }
    }

    /**
     * Delete file from bucket
     * @param string $file
     */
    protected function deleteFile($file)
    {
        $this->fileStorageBucket->deleteFile($file);

        // delete thumbnail images
        if (isset($this->transformImage['thumbnail'])) {
            $sizes = array_merge(array_keys($this->transformImage['thumbnail']), ['preview']);
            foreach ($sizes as $size) {
                $parts = pathinfo($file);
                $filename = $parts['filename'] . '_' . $size . '.' . $parts['extension'];
                $this->fileStorageBucket->deleteFile($filename);
            }
        }

        return true;
    }

    /**
     * Get relation model
     * @param mixed $model
     * @return string the result
     */
    protected function getRelationModel($model)
    {
        $relation = $model->getRelation($this->relation);
        $relationClassName = $relation->modelClass;
        $relationModel = new $relationClassName();
        $this->attachBehaviorToRelationModel($relationModel);

        return $relationModel;
    }
}
