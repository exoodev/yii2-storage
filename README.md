Example application configuration:

```php
'components' => [
    'fileStorage' => [
        'class' => 'exoo\storage\FileStorage',
        'storage' => [
            'class' => 'yii2tech\filestorage\local\Storage',
            'basePath' => '@storage',
            'dirPermission' => 0775,
            'filePermission' => 0755,
            'buckets' => [
                'files' => [
                    'baseSubPath' => '_files',
                    'fileSubDirTemplate' => '{ext}/{^name}/{^^name}',
                ],
                'images' => [
                    'baseSubPath' => '_images',
                    'fileSubDirTemplate' => '{^name}/{^^name}/{^^^name}',
                ],
            ]
        ],
        'actions' => [
            'storage' => [
                'file' => [
                    'bucketName' => 'files',
                    'sortable' => true,
                ],
                'image' => [
                    'bucketName' => 'images',
                    'sortable' => true,
                    'transformImage' => [
                        'thumbnail' => [
                            'large' => ['width' => 1000, 'height' => null],
                            'medium' => ['width' => 300, 'height' => null],
                            'small' => ['width' => 150, 'height' => null],
                        ],
                    ],
                ],
            ],
        ],
    ]
]

```
Example controller configuration:

```php
public function actions()
{
    return [
        'file' => [
            'class' => 'exoo\storage\actions\FileAction',
            'modelClass' => 'exoo\shop\models\Product',
            'bucketName' => 'shop',
        ],
        'image' => [
            'class' => 'exoo\storage\actions\FileAction',
            'modelClass' => 'exoo\shop\models\Product',
            'bucketName' => 'shop',
            'transformImage' => [
                'thumbnail' => [
                    'medium' => [
                        'width' => 250,
                        'height' => 250,
                    ],
                    'small' => [
                        'width' => 100,
                        'height' => 100,
                    ],
                ],
                'watermark' => [
                    'watermarkImage' => '@web/images/logo/watermark_logo.png',
                    'start' => [100, 100],
                    'thumbnail' => 'medium'
                ]
            ],
            'relation' => 'images',
            'relationAttribute' => 'product_id',
            'sortable' => true,
        ],
    ];
}
```

Example behavior configuration for model:


```php
public function behaviors()
{
    return [
        'file' => [
            'class' => FileBehavior::className(),
            'relation' => 'images',
            'relationAttribute' => 'category_id',
            'bucketName' => 'shop',
            'transformImage' => [
                'thumbnail' => [
                    'medium' => [
                        'width' => 250,
                        'height' => 250,
                    ],
                    'small' => [
                        'width' => 100,
                        'height' => 100,
                    ],
                ],
                'watermark' => [
                    'watermarkImage' => '@web/images/logo/watermark_logo.png',
                    'start' => [100, 100],
                    'thumbnail' => 'medium'
                ]
            ],
        ]
    ];
}
```

Example widget configuration:

```php
use exoo\widgets\FileInput;

<?= $form->field($post, 'filename')->widget(FileInput::className(), [
    'url' => ['image'],
    'sortable' => true,
    'clientOptions' => [
        'mime' => 'image/*',
        'cropperOptions' => [
            'aspectRatio' => 16 / 9
        ]
    ]
]) ?>
```
